package pubsub_channels

import (
	"context"
	"log"
	"reflect"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
)

const testProject = "test-project"

var setupClient *pubsub.Client

func init() {
	ctx := context.Background()
	timeoutCtx, cancel := context.WithTimeout(ctx, 3*time.Second)
	defer cancel()

	client, err := pubsub.NewClient(timeoutCtx, testProject)
	if err != nil {
		log.Fatalf("could not instantiate gcp test client: %s", err)
	}

	log.Println("created setup client")

	setupClient = client
}

func TestSubChan(t *testing.T) {
	setupCtx, setupCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer setupCancel()

	sourceTopic := setupTestSubs(setupCtx, setupClient)

	subIDs := []string{"sub1", "sub2"}
	options := Options{
		ReceiveSettings: pubsub.ReceiveSettings{
			MaxOutstandingMessages: 1,
		},
	}

	ctx := context.Background()
	pubSubChannels, err := NewPubSubChannels(ctx, testProject, subIDs, options)

	if err != nil {
		log.Fatalf("error instantiating PubSubChannels: %s", err)
	}

	cancelCtx, cancel := context.WithCancel(ctx)
	defer cancel()
	subChan, _ := pubSubChannels.NewSubChan(cancelCtx)

	act := make(map[string]string, 0)

	// the done channel will be closed as soon as we get 2 messages
	done := make(chan struct{})

	go func(c <-chan SubMessage) {
		subMsg := <-c
		act[subMsg.SubscriptionID] = string(subMsg.Message.Data)
		subMsg = <-c
		act[subMsg.SubscriptionID] = string(subMsg.Message.Data)
		close(done)
	}(subChan)

	select {
	case <-done:
	case <-time.After(time.Second):
		log.Printf("message reception timeout")
	}

	exp := map[string]string{
		"sub1": "test data",
		"sub2": "test data",
	}

	if !reflect.DeepEqual(act, exp) {
		t.Errorf("exp %v, got %v", exp, act)
	}

	// After we cancel, we shouldn't get any new updates
	cancel()
	// The cancellation takes some time
	time.Sleep(time.Second)

	newMsgCount := 0
	sourceTopic.Publish(ctx, &pubsub.Message{Data: []byte("new test data")})

	done = make(chan struct{})
	go func(c <-chan SubMessage) {
		_ = <-c
		newMsgCount++
		close(done)
	}(subChan)

	select {
	case <-done:
	case <-time.After(time.Second):
		log.Printf("message reception timeout")
	}

	if newMsgCount > 0 {
		t.Errorf("exp no new message, got %d", newMsgCount)
	}
}

func TestPubChan(t *testing.T) {
	setupCtx, setupCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer setupCancel()

	targetSub := setupTestPubs(setupCtx, setupClient)

	ctx := context.Background()
	subs := make([]string, 0)
	pubSubChannels, err := NewPubSubChannels(ctx, testProject, subs, Options{})

	if err != nil {
		log.Fatalf("error instantiating PubSubChannels: %s", err)
	}

	pubChan := pubSubChannels.NewPubChan()

	done := make(chan struct{})
	results := make([]string, 0)

	go func() {
		err = targetSub.Receive(ctx, func(cctx context.Context, message *pubsub.Message) {
			results = append(results, string(message.Data))
			done <- struct{}{}
		})

		if err != nil {
			log.Printf("got error while trying to receive messages: %s", err)
		}
	}()

	var successMsg string

	pubChan <- PubMessage{
		TopicID: "target-topic",
		Message: &pubsub.Message{Data: []byte("test data")},
		ErrCallback: func(msgID string, err error) {
			t.Errorf("unexpected error callback was called: %s", err)
		},
		SuccessCallback: func(msgID string) {
			successMsg = "success!"
		},
	}

	select {
	case <-done:
	case <-time.After(time.Second):
		log.Printf("message reception timeout")
	}

	exp := []string{"test data"}

	if !reflect.DeepEqual(results, exp) {
		t.Errorf("exp %v, got %v", exp, results)
	}

	if successMsg != "success!" {
		t.Errorf("expected success! message, got %s", successMsg)
	}

	var errorMsg string

	pubChan <- PubMessage{
		TopicID: "wrong-topic",
		Message: &pubsub.Message{Data: []byte("test data")},
		ErrCallback: func(msgID string, err error) {
			log.Println(err)
			errorMsg = err.Error()
		},
		SuccessCallback: func(msgID string) {
			t.Errorf("Unexpected success")
		},
	}

	select {
	case <-done:
	case <-time.After(time.Second):
		log.Printf("message reception timeout")
	}

	if errorMsg == "" {
		t.Error("expected message")
	}

	close(done)
}

func setupTestSubs(ctx context.Context, setupClient *pubsub.Client) *pubsub.Topic {
	sourceTopic := getOrCreateTopic(ctx, setupClient, "source-topic")

	log.Println("created subscription topic")

	getOrCreateSubscription(ctx, setupClient, sourceTopic, "sub1")
	getOrCreateSubscription(ctx, setupClient, sourceTopic, "sub2")

	log.Println("created subscriptions")

	sourceTopic.Publish(ctx, &pubsub.Message{Data: []byte("test data")})

	log.Println("published test data")

	return sourceTopic
}

func setupTestPubs(ctx context.Context, setupClient *pubsub.Client) *pubsub.Subscription {
	targetTopic := getOrCreateTopic(ctx, setupClient, "target-topic")
	log.Println("created publication topic")
	targetSub := getOrCreateSubscription(ctx, setupClient, targetTopic, "target-sub")
	log.Println("created target subscription")

	return targetSub
}

func getOrCreateTopic(ctx context.Context, setupClient *pubsub.Client, topicID string) *pubsub.Topic {
	topic := setupClient.TopicInProject(topicID, testProject)
	exists, err := topic.Exists(ctx)
	if err != nil {
		log.Fatalf("error checking if topic exists: %s", err)
	}

	if !exists {
		topic, err = setupClient.CreateTopic(ctx, topicID)
		if err != nil {
			log.Fatalf("failed at creating test topic: %s", err)
		}
	}
	return topic
}

func getOrCreateSubscription(ctx context.Context, setupClient *pubsub.Client, topic *pubsub.Topic, subscriptionID string) *pubsub.Subscription {
	sub := setupClient.SubscriptionInProject(subscriptionID, testProject)
	exists, err := sub.Exists(ctx)
	if err != nil {
		log.Fatalf("error checking if subscription exists: %s", err)
	}

	if !exists {
		sub, err = setupClient.CreateSubscription(ctx, subscriptionID, pubsub.SubscriptionConfig{
			Topic: topic,
		})

		if err != nil {
			log.Fatalf("could not create %s subscription: %s", subscriptionID, err)
		}
	}

	return sub
}
