# Overview

pubsub-channels is a tiny library which aims at hiding the usage of GCP's Pub/Sub behind a channel interface

Given a list of subscriptions, pubsub-channels exposes 2 channels:
- a subscription channel from which to read. It contains messages from the list of subscriptions, with the name of their source subscription.
- a publication channel to which to write. It expects messages with their target topic.

# Usage

A basic example can be found in [the example file](example.go)
