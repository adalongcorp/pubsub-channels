package examples

import (
	"context"
	"fmt"
	"log"
	"time"

	"cloud.google.com/go/pubsub"
	"google.golang.org/api/option"

	psc "gitlab.com/adalongcorp/pubsub-channels"
)

func example() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	options := psc.Options{
		// These options will be passed to the underlying Pub Sub client
		ClientOptions:   []option.ClientOption{option.WithAPIKey("xyz")},
		// These settings will be used when receiving messages
		ReceiveSettings: pubsub.ReceiveSettings{MaxOutstandingMessages: 5},
		// These settings will be used when publishing messages
		PublishSettings: pubsub.PublishSettings{Timeout: time.Minute},
	}

	pubSubChannels, err := psc.NewPubSubChannels(ctx, "my-project", []string{"sub1", "sub2"}, options)

	if err != nil {
		log.Fatalf("error instantiating PubSubChannels: %s", err)
	}

	// when cancelling the context, we stop listening to the subscriptions
	subCtx, subCancel := context.WithCancel(context.Background())
	// subChan contains messages coming from all channels
	subChan, errChan := pubSubChannels.NewSubChan(subCtx)

	pubChan := pubSubChannels.NewPubChan()

	go func() {
		for sourceMsg := range subChan {
			newData := []byte(fmt.Sprintf("%s from %s", sourceMsg.Message.Data, sourceMsg.SubscriptionID))

			// a PubMessage bundles a Message with its target topic
			// it will be sent to it if it exists
			// it also contains success and failure callbacks
			targetMsg := psc.PubMessage{
				TopicID:         "targetTopic",
				Message:         &pubsub.Message{Data: newData},
				ErrCallback: func(msgID string, err error) {
					log.Printf("error sending message %s: %s\n", msgID, err)
					sourceMsg.Message.Nack()
				},
				SuccessCallback: func(msgID string) {
					log.Printf("successfully sent message %s\n", msgID)
					sourceMsg.Message.Ack()
				},
			}

			pubChan <- targetMsg
		}
	}()

	go func() {
		for err := range errChan {
			log.Printf("error while reading message: %s", err)
		}
	}()

	time.Sleep(time.Minute)

	// stop receiving messages
	subCancel()
}
