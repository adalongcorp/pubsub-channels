package pubsub_channels

import (
	"context"
	"fmt"
	"sync"

	"cloud.google.com/go/pubsub"
	"google.golang.org/api/option"
)

// SubMessage bundles a GCP message with its originating subscription id
type SubMessage struct {
	SubscriptionID string
	Message        *pubsub.Message
}

// PubMessage bundles a GCP message with its target topic id
type PubMessage struct {
	TopicID         string
	Message         *pubsub.Message
	ErrCallback     func(msgID string, err error)
	SuccessCallback func(msgID string)
}

// PubSubChannels wraps a pubsub.Client behind a channel interface.
type PubSubChannels struct {
	client        *pubsub.Client
	subscriptions []*pubsub.Subscription
	topics        map[string]*pubsub.Topic
	options       Options
	topicsMux     sync.Mutex
}

// Options contains creation options for the gcp client, the topics and the subscriptions
type Options struct {
	// ClientOptions is the options that are passed to the underlying GCP client on creation
	ClientOptions   []option.ClientOption
	// ReceiveSettings are the settings that are passed to the pubsub.Subscription on creation
	ReceiveSettings pubsub.ReceiveSettings
	// PublishSettings are the settings that are passed to the pubsub.Topic on creation
	PublishSettings pubsub.PublishSettings
}

// NewPubSubChannels creates a new *PubSubChannels
// The ctx is only used to instantiate the GCP client
// It returns an error if the GCP instantiation fails
func NewPubSubChannels(ctx context.Context, projectID string, subscriptionIDs []string, options Options) (*PubSubChannels, error) {
	client, err := pubsub.NewClient(ctx, projectID, options.ClientOptions...)

	if err != nil {
		return nil, fmt.Errorf("error instantiating GCP Pub / Sub client: %s", err)
	}

	subscriptions := make([]*pubsub.Subscription, len(subscriptionIDs))

	for i, id := range subscriptionIDs {
		sub := client.Subscription(id)
		sub.ReceiveSettings = options.ReceiveSettings
		subscriptions[i] = sub
	}

	topics := make(map[string]*pubsub.Topic)

	return &PubSubChannels{
		client:        client,
		subscriptions: subscriptions,
		topics:        topics,
	}, nil
}

func (c *PubSubChannels) NewSubChan(ctx context.Context) (<-chan SubMessage, <-chan error) {
	// make the chan size the same as the max in-flight messages allowed by the client
	subChan := make(chan SubMessage, c.options.ReceiveSettings.MaxOutstandingMessages)

	errChan := make(chan error, len(c.subscriptions))

	for _, sub := range c.subscriptions {
		go func(s *pubsub.Subscription) {
			err := s.Receive(ctx, func(cctx context.Context, message *pubsub.Message) {
				subMsg := SubMessage{
					SubscriptionID: s.ID(),
					Message:        message,
				}

				subChan <- subMsg
			})
			if err != nil {
				errChan <- err
			}
		}(sub)
	}

	return subChan, errChan
}

func (c *PubSubChannels) NewPubChan() chan<- PubMessage {
	pubChan := make(chan PubMessage, c.options.ReceiveSettings.MaxOutstandingMessages)

	// ctx is required in topic.Publish signature, but it doesn't look that it is used anywhere, so pass a simple context
	ctx := context.Background()

	go func() {
		for pubMessage := range pubChan {
			topicID := pubMessage.TopicID

			topic, prs := c.topics[topicID]
			if !prs {
				topic = c.client.Topic(topicID)
				topic.PublishSettings = c.options.PublishSettings
				c.topicsMux.Lock()
				c.topics[topicID] = topic
				c.topicsMux.Unlock()
			}

			result := topic.Publish(ctx, pubMessage.Message)
			go func(pubMsg PubMessage, res *pubsub.PublishResult) {
				// it looks like the timeout is controlled by the ReceiveSetting's Timeout, so no real need to handle it here
				// by passing an expiring context
				id, err := res.Get(ctx)
				if err != nil {
					pubMsg.ErrCallback(id, err)
				} else {
					pubMsg.SuccessCallback(id)
				}
			}(pubMessage, result)
		}
	}()

	return pubChan
}
