module gitlab.com/adalongcorp/pubsub-channels

go 1.14

require (
	cloud.google.com/go/pubsub v1.9.0
	google.golang.org/api v0.36.0
)
